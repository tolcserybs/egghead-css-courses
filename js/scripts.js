const generateBox = (title, overview, poster_path, url, mainClass) => `
        <div ${mainClass ? `class=${mainClass}` : ""}>
            <h3 class="movie-title">${title}</h3>
            <div class="img-wrapper"><img src="${poster_path}"></div>
            <div class="movie-desc">${overview}</div>
            ${ url ? `<a href="${url}" target="_blank"></a>` : ""}
        </div>
    `;

const generateList = moviesList => //'<div class="mov-item"></div>'.repeat(12); /*`
    `${moviesList.map(
        movie => generateBox(
            movie.title,
            movie.overview,
            movie.poster_path,
            movie.url,
            "movie-item"
        )
    ).reduce((all, movieBlock) => all + movieBlock)}
`;

function generatePage(data) {
    const myData = formatData(data);

    const contentWrapper = document.getElementById("content-wrapper"); 
    contentWrapper.innerHTML = generateList(myData);
}

function formatData(data) {
    console.log(JSON.parse(data));
    const dataParsed = JSON.parse(data)
        .results
        .map(({
            title, 
            overview,
            poster_path
        }) => {
            return {
                title, 
                overview,
                poster_path: `https://image.tmdb.org/t/p/w1280${poster_path}`
            }
        });
    return dataParsed;
}

function getDatas(listID) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `https://api.themoviedb.org/4/list/${listID}?page=1&api_key=437f9fefd2fffd1ccefe1933f735cd34`);
    xhr.send();
    
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            generatePage(this.responseText);
        }
    };
}

getDatas(3);





